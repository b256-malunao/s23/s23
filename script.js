//console.log("Activity S23")

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pickachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
	talk: function() {

		console.log(this.pokemon[0] + "! I choose you!");
	}
}
console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

console.log("Result of talk method")
trainer.talk();


function Pokemon(name, level, health, attack, tackle) {
	this.name = name;
	this.level = level;
	this.health = health;
	this.attack = attack;
	this.tackle = Pokemon.tackle = function(chosenPokemon, targetPokemon) {
  	targetPokemonNewHealth = targetPokemon.health - chosenPokemon.attack;
 	console.log(chosenPokemon.name + " tackled " + targetPokemon.name);
 	targetPokemon.health = targetPokemonNewHealth;
 	console.log(targetPokemon.name + "'s health is now reduced to " + targetPokemon.health);
 	
 	Pokemon.faint = function() {
 	if(targetPokemon.health <= 0){
 		console.log(targetPokemon.name + " has fainted.");
 		console.log(targetPokemon);
 	} else {
 		console.log(targetPokemon);
 	}
 	
 } 
 }
}


let pickachu = new Pokemon("Pickachu", 12, 24, 12)
let geodude = new Pokemon("Geodude", 8, 16, 8)
let mewtwo = new Pokemon("Mewtwo", 100, 200, 100)
console.log(pickachu);
console.log(geodude);
console.log(mewtwo);


 // Pokemon.tackle = function(chosenPokemon, targetPokemon) {
 //  	targetPokemonNewHealth = targetPokemon.health - chosenPokemon.attack;
 // 	console.log(chosenPokemon.name + " tackled " + targetPokemon.name);
 // 	targetPokemon.health = targetPokemonNewHealth;
 // 	console.log(targetPokemon.name + "'s health is now reduced to " + targetPokemon.health);
 	
 // 	Pokemon.faint = function() {
 // 	if(targetPokemon.health <= 0){
 // 		console.log(targetPokemon.name + " has fainted.");
 // 		console.log(targetPokemon);
 // 	} else {
 // 		console.log(targetPokemon);
 // 	}
 	
 // } 
 // }

Pokemon.tackle(geodude, pickachu);
Pokemon.faint()

Pokemon.tackle(mewtwo,geodude);
Pokemon.faint()

Pokemon.tackle(pickachu, mewtwo);
Pokemon.faint()

	

// let target2 = pokemon1;
// pokemon2.tackle()
// console.log(pokemon2.name + " tackled " + target2.name);
// 	console.log(target2.name +"'s health is now reduced to " + newHealth);

// pokemon2.health = newHealth2;	
// console.log(pokemon2);